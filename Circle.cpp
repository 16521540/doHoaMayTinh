#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	
	SDL_RenderDrawPoint(ren, xc+x, yc+y);//1
	SDL_RenderDrawPoint(ren, xc-x, yc+y);//2
	SDL_RenderDrawPoint(ren, xc+x, yc-y);//3
	SDL_RenderDrawPoint(ren, xc-x, yc-y);//4
	SDL_RenderDrawPoint(ren, xc+y, yc+x);//5
	SDL_RenderDrawPoint(ren, xc-y, yc+x);//6
	SDL_RenderDrawPoint(ren, xc+y, yc-x);//7
	SDL_RenderDrawPoint(ren, xc-y, yc-x);//8
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int p = 3 - 2 * R;
	Draw8Points(xc, yc, x, y, ren);
	while(x<y)
	{
		if (p < 0)
		{
			p += 4 * x + 6;
		}
		else
		{
			p += 4 * (x - y) + 10;
			y-=1;
		}
		x += 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	

	int x = 0;
	int y = R;
	int p0 = 5/4 - R;
	/*int const1 = 2 * x + 3;
	int const2 = 2 * (x - y) + 5;*/

	Draw8Points(xc, yc, x, y, ren);

	while (x < y)
	{
		if (p0 < 0)
			p0 += 2*x+3;
		else
		{
			p0 += 2*(x-y)+5;
			y -= 1;
		}
		x += 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}
