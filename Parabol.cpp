#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	int p = 1 - A;
	while (x<A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y += 1;
		}
		x += 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2 * A - 1;
	while (y<800)
	{
		if (p > 0)
		{
			p += 4 * (A - x - 1);
			x += 1;
		}
		else
		{
			p += 4 * A;
		}
		y += 1;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	int p = -A;
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += -2 * A + 2 * x + 3;
			y -= 1;
		}
		x += 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2*A-1;
	while (y > -800)
	{
		if (p < 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * (A - x - 1);
			x += 1;
		}
		y -= 1;
		Draw2Points(xc, yc, x, y, ren);
	}

}